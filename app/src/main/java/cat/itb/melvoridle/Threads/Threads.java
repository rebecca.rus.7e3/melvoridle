package cat.itb.melvoridle.Threads;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cat.itb.melvoridle.R;

public class Threads extends Thread {
    private String textToShow;
    private TextView element;
    private ImageView imageView;
    private LinearLayout linearLayout;
    private Drawable imatge;

    public Threads(String textToShow, int image, TextView element, ImageView imageView, LinearLayout layout, Context context){

        this.textToShow = textToShow;
        this.element = element;
        this.imageView = imageView;
        this.linearLayout = layout;
        this.imatge= context.getResources().getDrawable(image);

        Thread thread = new Thread();
        thread.start();

    }

    @Override
    public void run() {
        super.run();
        linearLayout.setVisibility(View.VISIBLE);
        element.setText(textToShow);
        imageView.setImageDrawable(imatge);
        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        linearLayout.setVisibility(View.INVISIBLE);

    }
}
