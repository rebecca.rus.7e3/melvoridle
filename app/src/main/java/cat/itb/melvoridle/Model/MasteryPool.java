package cat.itb.melvoridle.Model;

import java.util.Arrays;
import java.util.List;

public class MasteryPool {
    int masteryxp;
    int masteryMax;
    List<Checkpoint> checkpointList;

    public MasteryPool() {
    }

    public MasteryPool(int masteryxp, int masteryMax, Checkpoint[] checkpoints) {
        this.masteryxp = masteryxp;
        this.masteryMax = masteryMax;
        checkpointList = Arrays.asList(checkpoints);
    }

    public int getMasteryxp() {
        return masteryxp;
    }

    public void setMasteryxp(int masteryxp) {
        this.masteryxp = masteryxp;
    }

    public int getMasteryMax() {
        return masteryMax;
    }

    public void setMasteryMax(int masteryMax) {
        this.masteryMax = masteryMax;
    }

    public List<Checkpoint> getCheckpointList() {
        return checkpointList;
    }

    public void setCheckpointList(List<Checkpoint> checkpointList) {
        this.checkpointList = checkpointList;
    }
}
