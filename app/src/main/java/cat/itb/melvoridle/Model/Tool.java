package cat.itb.melvoridle.Model;

import android.media.Image;

public class Tool extends Item{
    double bonusInterval;
    String profession;
    int lvlRequired;

    public Tool(String name, String description, int image, int price, double bonusInterval, String profession, int lvlRequired) {
        super(name, description, image, price, 1);
        this.bonusInterval = bonusInterval;
        this.profession = profession;
        this.lvlRequired = lvlRequired;
    }

    public Tool() {
    }

    public double getBonusInterval() {
        return bonusInterval;
    }

    public void setBonusInterval(double bonusInterval) {
        this.bonusInterval = bonusInterval;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public int getLvlRequired() {
        return lvlRequired;
    }

    public void setLvlRequired(int lvlRequired) {
        this.lvlRequired = lvlRequired;
    }


}
