package cat.itb.melvoridle.Model;

public class FishZone {
    private String zoneName;
    private String firstFishName;
    private String secondFishName;
    private String thirdFishName;
    private int experience;
    private int levelOfTheFish;

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public String getFirstFishName() {
        return firstFishName;
    }

    public void setFirstFishName(String firstFishName) {
        this.firstFishName = firstFishName;
    }

    public String getSecondFishName() {
        return secondFishName;
    }

    public void setSecondFishName(String secondFishName) {
        this.secondFishName = secondFishName;
    }

    public String getThirdFishName() {
        return thirdFishName;
    }

    public void setThirdFishName(String thirdFishName) {
        this.thirdFishName = thirdFishName;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getLevelOfTheFish() {
        return levelOfTheFish;
    }

    public void setLevelOfTheFish(int levelOfTheFish) {
        this.levelOfTheFish = levelOfTheFish;
    }

    public FishZone(String zoneName, String firstFishName, String secondFishName, String thirdFishName, int experience, int levelOfTheFish) {
        this.zoneName = zoneName;
        this.firstFishName = firstFishName;
        this.secondFishName = secondFishName;
        this.thirdFishName = thirdFishName;
        this.experience = experience;
        this.levelOfTheFish = levelOfTheFish;
    }
}
