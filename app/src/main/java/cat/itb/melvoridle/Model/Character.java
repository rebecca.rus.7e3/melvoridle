package cat.itb.melvoridle.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Character implements Parcelable {
    String idCharacter;
    String name;
    int combatLvl=3;
    int combatxp=0;
    int gp=0;
    List<Item> inventory;
    String idJugador;
    String gamemode;
    List<Skill> skillList;
    String saveType;

    public Character(String name, String idJugador, String gamemode, String saveType) {
        this.name = name;
        this.idJugador = idJugador;
        this.gamemode = gamemode;
        this.saveType=saveType;
        skillList = Arrays.asList(new SkillsViewModel().skillBank);
        inventory = new ArrayList<>(12);
    }

    public Character(String name, String gamemode, String saveType) {
        this.name = name;
        this.gamemode = gamemode;
        this.saveType=saveType;
        skillList = Arrays.asList(new SkillsViewModel().skillBank);
        inventory = new ArrayList<>(12);
        inventory.add(new Item("placeholder","description",0,0,1));
    }

    public Character() {
    }

    protected Character(Parcel in) {
        idCharacter = in.readString();
        name = in.readString();
        combatLvl = in.readInt();
        combatxp = in.readInt();
        gp = in.readInt();
        idJugador = in.readString();
        gamemode = in.readString();
        saveType = in.readString();
    }

    public static final Creator<Character> CREATOR = new Creator<Character>() {
        @Override
        public Character createFromParcel(Parcel in) {
            return new Character(in);
        }

        @Override
        public Character[] newArray(int size) {
            return new Character[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCombatLvl() {
        return combatLvl;
    }

    public void setCombatLvl(int combatLvl) {
        this.combatLvl = combatLvl;
    }

    public int getCombatxp() {
        return combatxp;
    }

    public void setCombatxp(int combatxp) {
        this.combatxp = combatxp;
    }


    public int getGp() {
        return gp;
    }

    public void setGp(int gp) {
        this.gp = gp;
    }

    public List<Item> getInventory() {
        return inventory;
    }

    public void setInventory(List<Item> inventory) {
        this.inventory = inventory;
    }

    public String getIdJugador() {
        return idJugador;
    }

    public void setIdJugador(String idJugador) {
        this.idJugador = idJugador;
    }

    public String getIdCharacter() {
        return idCharacter;
    }

    public void setIdCharacter(String idCharacter) {
        this.idCharacter = idCharacter;
    }

    public String getGamemode() {
        return gamemode;
    }

    public void setGamemode(String gamemode) {
        this.gamemode = gamemode;
    }

    public List<Skill> getSkillList() {
        return skillList;
    }

    public void setSkillList(List<Skill> skillList) {
        this.skillList = skillList;
    }

    public String getSaveType() {
        return saveType;
    }

    public void setSaveType(String saveType) {
        this.saveType = saveType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idCharacter);
        dest.writeString(name);
        dest.writeInt(combatLvl);
        dest.writeInt(combatxp);
        dest.writeInt(gp);
        dest.writeString(idJugador);
        dest.writeString(gamemode);
        dest.writeString(saveType);
    }
}
