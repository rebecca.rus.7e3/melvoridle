package cat.itb.melvoridle.Model;

public class Mineral {
    String name;
    String expAndTime;
    int level;
    int experienceToNextLevel;
    int expCap;

    int idImatgeArbre;


    public Mineral(String name, String expAndTime, int level, int experienceToNextLevel, int expCap, int imatgeArbre) {
        this.name = name;
        this.expAndTime = expAndTime;
        this.level = level;
        this.experienceToNextLevel = experienceToNextLevel;
        this.expCap = expCap;
        this.idImatgeArbre = imatgeArbre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExpAndTime() {
        return expAndTime;
    }

    public void setExpAndTime(String expAndTime) {
        this.expAndTime = expAndTime;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExperienceToNextLevel() {
        return experienceToNextLevel;
    }

    public void setExperienceToNextLevel(int experienceToNextLevel) {
        this.experienceToNextLevel = experienceToNextLevel;
    }

    public int getExpCap() {
        return expCap;
    }

    public void setExpCap(int expCap) {
        this.expCap = expCap;
    }
}
