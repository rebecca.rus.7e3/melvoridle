package cat.itb.melvoridle.Model;

public class SkillsViewModel {
    Skill[] skillBank = {new Skill("Woodcutting",new Tool("Bronze Axe","No bonuses",0,1,0,"Woodcutting",1),new MasteryPool(0,4500000,
                    new Checkpoint[]{new Checkpoint(450000,"5% increased Woodcutting Mastery XP"),new Checkpoint(1125000,"+5% increased chance to receive double Logs per action"),new Checkpoint(2250000,"All Logs sell for +50% GP Value"),new Checkpoint(4275000,"When you receive a Birds Nest, always receive a base minimum of 2")})),
            new Skill("Fishing",new Tool("Bronze Fishing Rod","No bonuses",0,1,0,"Fishing",1),new MasteryPool(0,11500000,
                    new Checkpoint[]{new Checkpoint(1150000,"+5% increased Fishing Mastery XP"),new Checkpoint(2875000,"Receive no more Junk"),new Checkpoint(5750000,"+5% increased chance to get double Fish"),new Checkpoint(10925000,"When you catch a Special Item, there is a 25% chance to receive one extra Special Item. It is possible to receive a different item than the original")})),
            new Skill("Firemaking",new MasteryPool(0,4500000,
                    new Checkpoint[]{new Checkpoint(450000,"+5% increased Firemaking Mastery XP"),new Checkpoint(1125000,"10% decreased Burning interval"),new Checkpoint(2250000,"Receive GP equal to 25% of the value of the Log you are burning"),new Checkpoint(4275000,"+5% increased Global Mastery XP")})),
            new Skill("Cooking",new MasteryPool(0,8000000,
                    new Checkpoint[]{new Checkpoint(800000,"+5% increased Cooking Mastery XP"),new Checkpoint(2000000,"+5% increased chance to get double cooked Food"),new Checkpoint(4000000,"+10% chance to preserve raw food in Cooking"),new Checkpoint(7600000,"All Food heals for +10% HP")})),
            new Skill("Mining",new Tool("Bronze Pickaxe","No bonuses",0,1,0,"Mining",1),new MasteryPool(0,5500000,
                    new Checkpoint[]{new Checkpoint(550000,"+5% increased Mining Mastery XP"),new Checkpoint(1375000,"Reduced Ore respawn time by 10%"),new Checkpoint(2750000,"Reduce Mining interval by 0.2s"),new Checkpoint(5225000,"All Rocks gain +10 Max HP (Bonus applied on Rock Respawn)")})),
            new Skill("Smithing",new MasteryPool(0,57500000,
                    new Checkpoint[]{new Checkpoint(5750000,"+5% increased Smithing Mastery XP"),new Checkpoint(14375000,"+5% resource preservation chance for Smithing"),new Checkpoint(28750000,"+5% resource preservation chance for Smithing"),new Checkpoint(54625000,"+10% chance to double items in Smithing.")})),
            new Skill("Thieving",new MasteryPool(0,4000000,
                    new Checkpoint[]{new Checkpoint(400000,"5% increased Thieving Mastery XP"),new Checkpoint(1000000,"+10% increased success rate for all NPC"),new Checkpoint(2000000,"+10% chance to receive double items from Thieving"),new Checkpoint(3800000,"+100% increased gold from Thieving")})),
            new Skill("Farming",new MasteryPool(0,11000000,
                    new Checkpoint[]{new Checkpoint(1100000,"+5% increased Farming Mastery XP"),new Checkpoint(2750000,"Crops cannot die (Bonus applied when crop grows)"),new Checkpoint(5500000,"+5% increased Crop harvest"),new Checkpoint(10450000,"Reduced crop grow time by 10% (Bonus applied when crop is planted)")})),
            new Skill("Fletching",new MasteryPool(0,28000000,
                    new Checkpoint[]{new Checkpoint(2800000,"+5% increased Fletching Mastery XP"),new Checkpoint(7000000,"Produce 1 extra Javelin per Fletch (Applied to base quantity)"),new Checkpoint(14000000,"Produce 1 extra Gem-Tipped Bolt per Fletch (Applied to base quantity)"),new Checkpoint(26600000,"Reduce Fletching Interval by 0.2s")})),
            new Skill("Crafting",new MasteryPool(0,24000000,
                    new Checkpoint[]{new Checkpoint(2400000,"+5% increased Crafting Mastery XP"),new Checkpoint(6000000,"+5% resource preservation chance for Crafting."),new Checkpoint(12000000,"Decreased Crafting Interval by 0.2s"),new Checkpoint(22800000,"Always Craft a base quantity of 2 for Rings and Necklaces")})),
            new Skill("Runecrafting",new MasteryPool(0,42000000,
                    new Checkpoint[]{new Checkpoint(4200000,"+5% increased Runecrafting Mastery XP"),new Checkpoint(10500000,"Grants 250% base Runecrafting XP from Runes"),new Checkpoint(21000000,"+10% resource preservation chance for Runecrafting"),new Checkpoint(39900000,"Grants an extra 5 Runes per craft")})),
            new Skill("Herblore",new MasteryPool(0,12500000,
                    new Checkpoint[]{new Checkpoint(1250000,"+5% increased Herblore Mastery XP"),new Checkpoint(3125000,"+3% increased Herblore Skill XP"),new Checkpoint(6250000,"+5% resource preservation chance for Herblore"),new Checkpoint(11875000,"+10% chance to double Potions per action in Herblore")})),
            new Skill("Agility",new MasteryPool(0,23000000,
                    new Checkpoint[]{new Checkpoint(2300000,"+5% increased Agility Mastery XP"),new Checkpoint(5750000,"+10% GP from Agility"),new Checkpoint(11500000,"+10% Global Agility Obstacle cost reduction."),new Checkpoint(21850000,"+15% Agility Obstacle Item cost reduction")})),
            new Skill("Alt. Magic")

    };

    public SkillsViewModel() {
    }
}
