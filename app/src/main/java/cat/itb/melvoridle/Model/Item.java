package cat.itb.melvoridle.Model;

import android.media.Image;

public class Item {
    String name;
    String description;
    int imageID;
    int price;
    int quantity;

    public Item() {
    }

    public Item(String name, String description, int imageID, int price, int quantity) {
        this.name = name;
        this.description = description;
        this.imageID = imageID;
        this.price = price;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
