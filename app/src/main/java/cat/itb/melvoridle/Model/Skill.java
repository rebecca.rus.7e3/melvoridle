package cat.itb.melvoridle.Model;

public class Skill {
    String name;
    int lvl;
    int xp;
    Tool currentTool;
    MasteryPool masteryPool;

    public Skill() {
    }

    public Skill(String name, Tool currentTool, MasteryPool masteryPool) {
        this.name = name;
        this.currentTool = currentTool;
        this.masteryPool = masteryPool;
        lvl=1;
        xp=0;
    }

    public Skill(String name, MasteryPool masteryPool) {
        this.name = name;
        this.masteryPool = masteryPool;
        lvl=1;
        xp=0;
    }

    public Skill(String name) {
        this.name = name;
        lvl=1;
        xp=0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLvl() {
        return lvl;
    }

    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public Tool getCurrentTool() {
        return currentTool;
    }

    public void setCurrentTool(Tool currentTool) {
        this.currentTool = currentTool;
    }

    public MasteryPool getMasteryPool() {
        return masteryPool;
    }

    public void setMasteryPool(MasteryPool masteryPool) {
        this.masteryPool = masteryPool;
    }

    public int calculateXPCap(int level){
        return level*10000;
    }
    public double calculatePercentage(int xpCap, int xp){
        double xpCapDouble = (double)xpCap;
        double xpDouble = (double)xp;
        return (xpDouble/xpCapDouble)*100;

    }
}
