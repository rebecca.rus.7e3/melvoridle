package cat.itb.melvoridle.Model;

public class Checkpoint {
    int xpRequired;
    String effect;

    public Checkpoint(int xpRequired, String effect) {
        this.xpRequired = xpRequired;
        this.effect = effect;
    }

    public int getXpRequired() {
        return xpRequired;
    }

    public void setXpRequired(int xpRequired) {
        this.xpRequired = xpRequired;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public Checkpoint() {
    }
}
