package cat.itb.melvoridle.Firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import cat.itb.melvoridle.Model.Character;

public class FirebaseHelper {
    FirebaseDatabase database;
    DatabaseReference myRef;

    public FirebaseHelper() {
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("characters");
    }

    public void insert(Character character){
        String key = myRef.push().getKey();
        character.setIdCharacter(key);
        myRef.child(key).setValue(character);
    }

    public void update(Character character) {
        myRef.child(character.getIdCharacter()).setValue(character);
    }

    public void delete(Character character){
        myRef.child(character.getIdCharacter()).removeValue();
    }

    public FirebaseDatabase getDatabase() {
        return database;
    }

    public void setDatabase(FirebaseDatabase database) {
        this.database = database;
    }

    public DatabaseReference getMyRef() {
        return myRef;
    }

    public void setMyRef(DatabaseReference myRef) {
        this.myRef = myRef;
    }
}
