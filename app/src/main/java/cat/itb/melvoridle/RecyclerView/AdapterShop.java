package cat.itb.melvoridle.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.melvoridle.Model.Item;
import cat.itb.melvoridle.R;

public class AdapterShop extends RecyclerView.Adapter<AdapterShop.ShopViewHolder> {
    private AdapterShop.OnItemClickListener listener;
    List<Item> modelList;
    private int layout;

    public AdapterShop(AdapterShop.OnItemClickListener listener, List<Item> modelList, int layout) {
        this.listener = listener;
        this.modelList = modelList;
        this.layout = layout;
    }

    @NonNull
    @Override
    public ShopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout,parent,false);
        return new AdapterShop.ShopViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ShopViewHolder holder, final int position) {
        holder.itemName.setText(modelList.get(position).getName());
        holder.itemDesc.setText(modelList.get(position).getDescription());
        holder.itemImage.setImageResource(modelList.get(position).getImageID());
        holder.itemPrice.setText(modelList.get(position).getPrice()+"");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(modelList.get(position),position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }
    public interface OnItemClickListener {
        void onItemClick(Item i, int position);
    }


    public class ShopViewHolder extends RecyclerView.ViewHolder{
        ImageView itemImage;
        TextView itemName;
        TextView itemDesc;
        TextView itemPrice;
        public ShopViewHolder(@NonNull View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.itemShopName);
            itemImage = itemView.findViewById(R.id.imagenItem);
            itemDesc = itemView.findViewById(R.id.itemShopDesc);
            itemPrice = itemView.findViewById(R.id.shopItemPrice);

        }
    }
}
