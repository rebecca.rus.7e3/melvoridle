package cat.itb.melvoridle.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;

import cat.itb.melvoridle.Firebase.FirebaseHelper;
import cat.itb.melvoridle.Fragments.FishFragment;
import cat.itb.melvoridle.Fragments.TreeFragment;
import cat.itb.melvoridle.Model.FishZone;
import cat.itb.melvoridle.Model.Item;
import cat.itb.melvoridle.Model.Tree;
import cat.itb.melvoridle.R;

import static cat.itb.melvoridle.Fragments.TreeFragment.currentChar;


public class AdapterFishing extends  RecyclerView.Adapter<AdapterFishing.ViewHolder>{

    private Context context;

    private List<FishZone> fish;
    private int layout;
    private OnItemClickListener itemClickListener;
    private FishFragment fragment;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout,parent,false);
        ViewHolder vh = new ViewHolder(v,context,fragment);
        return vh;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(this.fish.get(position));
    }



    @Override
    public int getItemCount() {
        return this.fish.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private Context context;

        private TextView zoneName;
        private ImageView firstFish;
        private ImageView secondFish;
        private ImageView thirdFish;
        private TextView firstFishName;
        private TextView secondFishName;
        private TextView thirdFishName;
        private TextView nameOfTheFish;
        private ImageView pictureOfTheFish;
        private TextView expFishGives;
        private TextView levelOfTheFish;
        private List<Item> inventory;
        private FishFragment fragment;
        private FirebaseHelper firebaseHelper;

        public ViewHolder(@NonNull View itemView, Context context, final FishFragment fragment) {
            super(itemView);
            this.zoneName = itemView.findViewById(R.id.cut_text_cards);
            this.firstFish = itemView.findViewById(R.id.fish_icon_1);
            this.secondFish = itemView.findViewById(R.id.fish_icon_2);
            this.thirdFish = itemView.findViewById(R.id.fish_icon_3);
            this.firstFishName = itemView.findViewById(R.id.fish_text_1);
            this.secondFishName = itemView.findViewById(R.id.fish_text_2);
            this.thirdFishName = itemView.findViewById(R.id.fish_text_3);
            this.nameOfTheFish = itemView.findViewById(R.id.fish_name_info);
            this.pictureOfTheFish = itemView.findViewById(R.id.image_of_the_fish);
            this.expFishGives = itemView.findViewById(R.id.experience_per_fish);
            this.levelOfTheFish = itemView.findViewById(R.id.level_per_fish);

            this.context = context;
            inventory = currentChar.getInventory();
            firebaseHelper = new FirebaseHelper();
            this.fragment = fragment;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index =-1;
                    for (int i =0;i<inventory.size();i++){
                        if (inventory.get(i).getName().equalsIgnoreCase(nameOfTheFish.getText().toString())){
                            index=i;
                        }
                    }
                    if (index!=-1){
                        inventory.get(index).setQuantity(inventory.get(index).getQuantity()+1);

                    } else {
                        inventory.add(new Item(nameOfTheFish.getText().toString(),"No item description",R.drawable.ic_fishing_header,1,1));
                    }
                    currentChar.getSkillList().get(1).setXp(currentChar.getSkillList().get(1).getXp()+100);
                    firebaseHelper.update(currentChar);
                    fragment.updateUI();


                }
            });

        }
        public void bind(final FishZone fish ){
            this.nameOfTheFish.setText(fish.getFirstFishName());
            this.firstFishName.setText(fish.getFirstFishName());
            this.secondFishName.setText(fish.getSecondFishName());
            this.thirdFishName.setText(fish.getThirdFishName());
            this.expFishGives.setText(String.valueOf(fish.getExperience()));
            this.firstFish.setImageDrawable(context.getResources().getDrawable(R.drawable.shrimp));
            this.secondFish.setImageDrawable(context.getResources().getDrawable(R.drawable.shrimp));
            this.thirdFish.setImageDrawable(context.getResources().getDrawable(R.drawable.shrimp));
            this.pictureOfTheFish.setImageDrawable(context.getResources().getDrawable(R.drawable.shrimp));
            this.zoneName.setText("EXAMPLE");
            this.levelOfTheFish.setText(Integer.toString(fish.getLevelOfTheFish()));
        }

    }

    public AdapterFishing(Context context, List<FishZone> fish, int layout, OnItemClickListener itemClickListener, FishFragment fragment) {
        this.context = context;
        this.fish = fish;
        this.layout = layout;
        this.itemClickListener = itemClickListener;
        this.fragment = fragment;
    }

    public interface OnItemClickListener{
        void onItemClick(Tree tree, int position);
    }
}

