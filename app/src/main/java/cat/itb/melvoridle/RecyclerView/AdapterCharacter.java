package cat.itb.melvoridle.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.Query;

import java.util.List;

import cat.itb.melvoridle.Model.Character;
import cat.itb.melvoridle.R;

public class AdapterCharacter extends FirebaseRecyclerAdapter<Character, AdapterCharacter.ViewHolder> {
    private OnItemClickListener listener;
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_character, parent, false);
        return new ViewHolder(v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView saveType;
        TextView name;
        TextView money;
        TextView gamemode;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            saveType = itemView.findViewById(R.id.saveType);
            name = itemView.findViewById(R.id.characterName);
            money = itemView.findViewById(R.id.characterMoney);
            gamemode = itemView.findViewById(R.id.characterGamemode);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION && listener != null) {
                        listener.onItemClick(getItem(position), position);
                    }
                }
            });
        }
    }

    public AdapterCharacter(@NonNull FirebaseRecyclerOptions<Character> options) {
        super(options);
    }

    @Override
    protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull Character model) {
        holder.saveType.setText(model.getSaveType());
        holder.name.setText(model.getName());
        holder.money.setText(model.getGp()+"GP");
        holder.gamemode.setText(model.getGamemode());
    }

    public interface OnItemClickListener {
        void onItemClick(Character c, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
