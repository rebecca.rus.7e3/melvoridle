package cat.itb.melvoridle.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.List;

import cat.itb.melvoridle.Model.Item;
import cat.itb.melvoridle.R;

public class AdapterInventory extends RecyclerView.Adapter<AdapterInventory.InventoryHolder> {
    private OnItemClickListener listener;
    List<Item> modelList;
    private int layout;

    public AdapterInventory(OnItemClickListener listener, List<Item> modelList, int layout) {
        this.listener = listener;
        this.modelList = modelList;
        this.layout = layout;
    }

    @NonNull
    @Override
    public InventoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout,parent,false);
        return new InventoryHolder(v);
    }



    @Override
    public void onBindViewHolder(@NonNull InventoryHolder holder, final int position) {
        holder.itemQuantity.setText(modelList.get(position).getQuantity()+"");
        holder.itemImage.setImageResource(modelList.get(position).getImageID());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(modelList.get(position),position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Item i, int position);
    }


    public class InventoryHolder extends RecyclerView.ViewHolder{
        ImageView itemImage;
        TextView itemQuantity;

        public InventoryHolder(@NonNull View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.item_image);
            itemQuantity = itemView.findViewById(R.id.item_quantity);
        }
    }
}
