package cat.itb.melvoridle.RecyclerView;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cat.itb.melvoridle.Firebase.FirebaseHelper;
import cat.itb.melvoridle.Fragments.MiningFragment;
import cat.itb.melvoridle.Model.Item;
import cat.itb.melvoridle.Model.Mineral;
import cat.itb.melvoridle.Model.Tree;
import cat.itb.melvoridle.R;

import static cat.itb.melvoridle.Fragments.TreeFragment.currentChar;


public class AdapterMining extends  RecyclerView.Adapter<AdapterMining.ViewHolder>{

    private Context context;

    private List<Mineral> minerals;
    private int layout;

    private MiningFragment fragment;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout,parent,false);
        ViewHolder vh = new ViewHolder(v,context, fragment);
        return vh;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(this.minerals.get(position));
    }



    @Override
    public int getItemCount() {
        return this.minerals.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private Context context;

        private TextView mineralName;
        private TextView expAndTime;
        private ImageView mineralImage;
        private ProgressBar progressBarTallarArbre;
        private TextView levelNumber;
        private TextView experience;
        private TextView experienceToNextLevel;
        private ProgressBar experienceToNextLevelProgress;
        private OnItemClickListener itemClickListener;
        private List<Item> inventory;
        private FirebaseHelper firebaseHelper;
        private MiningFragment fragment;

        public ViewHolder(@NonNull View itemView, Context context, final MiningFragment fragment) {
            super(itemView);
            this.mineralName = itemView.findViewById(R.id.name_of_the_mineral);
            this.expAndTime = itemView.findViewById(R.id.mining_time);
            this.mineralImage = itemView.findViewById(R.id.image_of_the_mineral);
            this.progressBarTallarArbre = itemView.findViewById(R.id.progress_bar_remaining_mineral);
            this.levelNumber = itemView.findViewById(R.id.level_number_mining);
            this.experience = itemView.findViewById(R.id.mining_experience);
            this.experienceToNextLevel = itemView.findViewById(R.id.experience_cap_mining);
            this.experienceToNextLevelProgress = itemView.findViewById(R.id.progress_bar_mining_level);
            this.context = context;
            inventory = currentChar.getInventory();
            firebaseHelper = new FirebaseHelper();
            this.fragment = fragment;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index =-1;
                    for (int i =0;i<inventory.size();i++){
                        if (inventory.get(i).getName().equalsIgnoreCase(mineralName.getText().toString())){
                            index=i;
                        }
                    }
                    if (index!=-1){
                        inventory.get(index).setQuantity(inventory.get(index).getQuantity()+1);

                    } else {
                        inventory.add(new Item(mineralName.getText().toString(),"No item description",R.drawable.tree_logo,1,1));
                    }
                    currentChar.getSkillList().get(4).setXp(currentChar.getSkillList().get(4).getXp()+100);
                    firebaseHelper.update(currentChar);
                    fragment.updateUI();


                }
            });
        }
        public void bind(final Mineral mineral ){
            this.mineralName.setText(mineral.getName());
            this.expAndTime.setText(mineral.getExpAndTime());
            this.mineralImage.setImageDrawable(context.getResources().getDrawable(R.drawable.rock_copper));
            this.levelNumber.setText(Integer.toString(mineral.getLevel()));
            this.experience.setText(Integer.toString(mineral.getExperienceToNextLevel()));
            this.experienceToNextLevel.setText(Integer.toString(mineral.getExpCap()));
        }

    }

    public AdapterMining(Context context, List<Mineral> minerals, int layout, OnItemClickListener itemClickListener, MiningFragment fragment) {
        this.context = context;
        this.minerals= minerals;
        this.layout = layout;
        this.fragment = fragment;
    }

    public interface OnItemClickListener{
        void onItemClick(Tree tree, int position);
    }
}

