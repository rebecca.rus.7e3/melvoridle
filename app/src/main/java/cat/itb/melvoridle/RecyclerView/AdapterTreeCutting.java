package cat.itb.melvoridle.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;
import java.util.List;

import cat.itb.melvoridle.Firebase.FirebaseHelper;
import cat.itb.melvoridle.Fragments.TreeFragment;
import cat.itb.melvoridle.Model.Item;
import cat.itb.melvoridle.Model.Tree;
import cat.itb.melvoridle.R;
import cat.itb.melvoridle.Threads.Threads;

import static cat.itb.melvoridle.Fragments.TreeFragment.currentChar;


public class AdapterTreeCutting extends  RecyclerView.Adapter<AdapterTreeCutting.ViewHolder>{

    private Context context;

    private List<Tree> trees;
    private int layout;
    private OnItemClickListener itemClickListener;
    private TreeFragment fragment;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout,parent,false);
        ViewHolder vh = new ViewHolder(v,context, fragment);
        return vh;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(this.trees.get(position));
    }



    @Override
    public int getItemCount() {
        return this.trees.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        private Context context;

        private TextView treeName;
        private TextView expAndTime;
        private ImageView treeImage;
        private ProgressBar progressBarTallarArbre;
        private TextView levelNumber;
        private TextView experience;
        private TextView experienceToNextLevel;
        private ProgressBar experienceToNextLevelProgress;
        private List<Item> inventory;
        private TreeFragment fragment;
        private FirebaseHelper firebaseHelper;
        private TextView notificationText;
        private LinearLayout notificationLayout;
        private ImageView notificationImage;


        //TODO ACABAR THREADS

        public ViewHolder(@NonNull View itemView, final Context context, final TreeFragment fragment) {
            super(itemView);
            this.treeName = itemView.findViewById(R.id.name_of_the_tree);
            this.expAndTime = itemView.findViewById(R.id.cutting_time);
            this.treeImage = itemView.findViewById(R.id.image_of_the_tree);
            this.progressBarTallarArbre = itemView.findViewById(R.id.progress_bar_tree);
            this.levelNumber = itemView.findViewById(R.id.level_number);
            this.experience = itemView.findViewById(R.id.tree_experience);
            this.experienceToNextLevel = itemView.findViewById(R.id.experience_cap);
            this.experienceToNextLevelProgress = itemView.findViewById(R.id.progress_bar_tree_level);
            this.context = context;
            inventory = currentChar.getInventory();
            firebaseHelper = new FirebaseHelper();
            this.fragment = fragment;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index =-1;
                    for (int i =0;i<inventory.size();i++){
                        if (inventory.get(i).getName().equalsIgnoreCase(treeName.getText().toString())){
                            index=i;
                        }
                    }
                    if (index!=-1){
                        inventory.get(index).setQuantity(inventory.get(index).getQuantity()+1);

                    } else {
                        Item tree = new Item(treeName.getText().toString(),"No itemDescription",R.drawable.tree_logo,1,1);
                        inventory.add(tree);
                        switch(tree.getName()){
                            case "Arbre 1":
                                currentChar.getSkillList().get(0).setXp(currentChar.getSkillList().get(0).getXp()+100);
                                break;
                            case "Arbre 2":
                                currentChar.getSkillList().get(0).setXp(currentChar.getSkillList().get(0).getXp()+200);
                                break;
                            case "Arbre 3":
                                currentChar.getSkillList().get(0).setXp(currentChar.getSkillList().get(0).getXp()+300);
                                break;
                            case "Arbre 4":
                                currentChar.getSkillList().get(0).setXp(currentChar.getSkillList().get(0).getXp()+400);
                                break;
                            case "Arbre 5":
                                currentChar.getSkillList().get(0).setXp(currentChar.getSkillList().get(0).getXp()+500);
                                break;
                            case "Arbre 6":
                                currentChar.getSkillList().get(0).setXp(currentChar.getSkillList().get(0).getXp()+600);
                                break;
                            case "Arbre 7":
                                currentChar.getSkillList().get(0).setXp(currentChar.getSkillList().get(0).getXp()+700);
                                break;
                            default:
                                currentChar.getSkillList().get(0).setXp(currentChar.getSkillList().get(0).getXp()+800);
                                break;
                        }
                    }
                    firebaseHelper.update(currentChar);
                    fragment.updateUI();


                }
            });

        }
        public void bind(final Tree tree ){
            this.treeName.setText(tree.getName());
            this.expAndTime.setText(tree.getExpAndTime());
            this.treeImage.setImageDrawable(context.getResources().getDrawable(tree.getImatgeArbre()));
            this.levelNumber.setText(Integer.toString(tree.getLevel()));
            this.experience.setText(Integer.toString(tree.getExperienceToNextLevel()));
            this.experienceToNextLevel.setText(Integer.toString(tree.getExpCap()));
        }

    }

    public AdapterTreeCutting(Context context, List<Tree> trees, int layout, OnItemClickListener itemClickListener, TreeFragment fragment) {
        this.context = context;
        this.trees = trees;
        this.layout = layout;
        this.itemClickListener = itemClickListener;
        this.fragment = fragment;
    }

    public interface OnItemClickListener{
        void onItemClick(Tree tree, int position);
    }
}

