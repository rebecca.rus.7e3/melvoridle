package cat.itb.melvoridle.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cat.itb.melvoridle.Model.Character;
import cat.itb.melvoridle.Model.FishZone;
import cat.itb.melvoridle.Model.Item;
import cat.itb.melvoridle.Model.Skill;
import cat.itb.melvoridle.Model.Tree;
import cat.itb.melvoridle.R;
import cat.itb.melvoridle.RecyclerView.AdapterFishing;
import cat.itb.melvoridle.RecyclerView.AdapterTreeCutting;

public class FishFragment extends Fragment {

    private List<FishZone> fish;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private AdapterFishing.OnItemClickListener itemClickListener;

    public static Character currentChar;

    TextView skillLevel;
    TextView skillXp;
    TextView currentTool;
    ProgressBar progressBar_level;
    ProgressBar currentMasteryLevelProgress;
    TextView currentMasteryLevelNumber;

    List<Item> inventory;


    public FishFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fish,container,false);


        fish = exempleFish();

        recyclerView = v.findViewById(R.id.recycler_view_fishing);
        layoutManager = new LinearLayoutManager(getContext());
        adapter = new AdapterFishing(getContext(),fish,R.layout.item_view_fishing,new AdapterFishing.OnItemClickListener(){

            @Override
            public void onItemClick(Tree tree, int position) {

            }
        },this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        Bundle parametros = this.getArguments();

        skillLevel = v.findViewById(R.id.skill_level_level_fishing);
        skillXp = v.findViewById(R.id.skill_xp_xp_fishing);
        currentTool = v.findViewById(R.id.current_tool_name_fishing);
        progressBar_level = v.findViewById(R.id.current_level_progress_tree_fragment_fishing);
        currentMasteryLevelProgress = v.findViewById(R.id.current_mastery_level_progress_tree_fragment_fishing);
        currentMasteryLevelNumber = v.findViewById(R.id.current_mastery_level_number_fishing);


        if (parametros!=null){
            currentChar = parametros.getParcelable("character");
            inventory = currentChar.getInventory();
            List<Skill> skills = currentChar.getSkillList();
            skillLevel.setText(skills.get(1).getLvl()+"/99");
            skillXp.setText(skills.get(1).getXp()+" XP");
            currentTool.setText(skills.get(1).getCurrentTool().getName());
            int xpCap = skills.get(1).calculateXPCap(skills.get(1).getLvl());
            progressBar_level.setProgress((int) skills.get(1).calculatePercentage(xpCap,skills.get(1).getXp()));
        }
        return v;

    }
    public void updateUI() {
        if(currentChar.getSkillList().get(1).getXp()>=currentChar.getSkillList().get(1).calculateXPCap(currentChar.getSkillList().get(1).getLvl())&&currentChar.getSkillList().get(1).getLvl()!=100){
            currentChar.getSkillList().get(1).setLvl(currentChar.getSkillList().get(1).getLvl()+1);
            currentChar.getSkillList().get(1).setXp(0);
        }
        Skill treeCuttingSkill = currentChar.getSkillList().get(1);
        progressBar_level.setProgress((int) treeCuttingSkill.calculatePercentage(treeCuttingSkill.calculateXPCap(treeCuttingSkill.getLvl()),treeCuttingSkill.getXp()));
        skillXp.setText(treeCuttingSkill.getXp()+" XP");
        skillLevel.setText(treeCuttingSkill.getLvl()+"/99");
        currentChar.getSkillList().get(1).getMasteryPool().setMasteryMax(7000000);
        if(currentChar.getSkillList().get(1).getMasteryPool().getMasteryxp()!=currentChar.getSkillList().get(1).getMasteryPool().getMasteryMax()){
            currentChar.getSkillList().get(1).getMasteryPool().setMasteryxp(currentChar.getSkillList().get(1).getMasteryPool().getMasteryxp()+1);
            currentMasteryLevelNumber.setText(currentChar.getSkillList().get(1).getMasteryPool().getMasteryxp()+"/"+currentChar.getSkillList().get(1).getMasteryPool().getMasteryMax());
        }
        Skill treeSkill = currentChar.getSkillList().get(1);
        progressBar_level.setProgress((int) treeSkill.calculatePercentage(treeSkill.calculateXPCap(treeSkill.getLvl()),treeSkill.getXp()));
    }
    private List<FishZone> exempleFish(){
        return new ArrayList<FishZone>(){{
            add(new FishZone("Peix 1","Exemple 1","Exemple 10","Exemple 19",1000,R.drawable.fishing));
            add(new FishZone("Peix 2","Exemple 2","Exemple 11","Exemple 20",1000,R.drawable.fishing));
            add(new FishZone("Peix 3","Exemple 3","Exemple 12","Exemple 21",1000,R.drawable.fishing));
            add(new FishZone("Peix 4","Exemple 4","Exemple 13","Exemple 22",1000,R.drawable.fishing));
            add(new FishZone("Peix 5","Exemple 5","Exemple 14","Exemple 23",1000,R.drawable.fishing));
            add(new FishZone("Peix 6","Exemple 6","Exemple 15","Exemple 24",1000,R.drawable.fishing));
            add(new FishZone("Peix 7","Exemple 7","Exemple 16","Exemple 25",1000,R.drawable.fishing));
            add(new FishZone("Peix 8","Exemple 8","Exemple 17","Exemple 26",1000,R.drawable.fishing));
            add(new FishZone("Peix 9","Exemple 9","Exemple 18","Exemple 27",1000,R.drawable.fishing));
        }};
    }
}