package cat.itb.melvoridle.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import cat.itb.melvoridle.R;


public class LoginFragment extends Fragment {


TextView register;
Button loginButton;
TextInputLayout usernameLayout;
TextInputEditText usernameEditText;
TextInputLayout passwordLayout;
TextInputEditText passwordEditText;
private FirebaseAuth mAuth;

    public LoginFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        register = v.findViewById(R.id.RegisterButton);
        loginButton = v.findViewById(R.id.loginButton);
        usernameLayout = v.findViewById(R.id.input_layout_username_login);
        passwordLayout = v.findViewById(R.id.input_layout_password_login);
        usernameEditText =v.findViewById(R.id.editTextUserNameLogin);
        passwordEditText = v.findViewById(R.id.editTextPasswordLogin);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (checkLogin()){
                    String email = usernameEditText.getText().toString();
                    String password = passwordEditText.getText().toString();
                    mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(getContext(),"Login completado",Toast.LENGTH_SHORT).show();
                                NavDirections loginToHome = LoginFragmentDirections.loginToHome(true);
                                Navigation.findNavController(v).navigate(loginToHome);
                            } else {
                                Log.w("tag","signInWithEmail:failure",task.getException());
                                Toast.makeText(getContext(),"Login NO completado",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDirections loginToRegister = LoginFragmentDirections.loginToRegister();
                Navigation.findNavController(v).navigate(loginToRegister);
            }
        });

        return v;
    }

    public boolean checkLogin(){
        if (usernameEditText.getText().toString().isEmpty()){
            usernameEditText.setError("Campo vacio!!!");
            return false;
        } else {
            if (passwordEditText.getText().toString().isEmpty()){
                passwordLayout.setError("Campo vacio!!!");
                return false;
            } else {
                passwordLayout.setError("");
                return true;
            }
        }
    }
}