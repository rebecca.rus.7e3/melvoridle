package cat.itb.melvoridle.Fragments;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cat.itb.melvoridle.Model.Character;
import cat.itb.melvoridle.Model.Item;
import cat.itb.melvoridle.Model.Skill;
import cat.itb.melvoridle.Model.Tree;
import cat.itb.melvoridle.R;
import cat.itb.melvoridle.RecyclerView.AdapterTreeCutting;

public class TreeFragment extends Fragment {

    private List<Tree> trees;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    DrawerLayout drawerLayout;
    public static Character currentChar;

    TextView skillLevel;
    TextView skillXp;
    TextView currentTool;
    ProgressBar progressBar_level;
    ProgressBar currentMasteryLevelProgress;
    TextView currentMasteryLevelNumber;

   List<Item> inventory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_trees,container,false);
        trees = exampleTrees();
        adapter = new AdapterTreeCutting(getContext(),trees,R.layout.item_view_woodcutting,new AdapterTreeCutting.OnItemClickListener(){
            @Override
            public void onItemClick(Tree tree, int position) {

            }
        }, this);
        recyclerView = v.findViewById(R.id.recycler_view_trees);
        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);

        Bundle parametros = this.getArguments();

        skillLevel = v.findViewById(R.id.skill_level_level);
        skillXp = v.findViewById(R.id.skill_xp_xp);
        currentTool = v.findViewById(R.id.current_tool_name);
        progressBar_level = v.findViewById(R.id.progress_bar_tree_level);
        currentMasteryLevelProgress = v.findViewById(R.id.current_mastery_level_progress_tree_fragment_tress);
        currentMasteryLevelNumber = v.findViewById(R.id.current_mastery_level_number_trees);


        if (parametros!=null){
            currentChar = parametros.getParcelable("character");
            inventory = currentChar.getInventory();
            List<Skill> skills = currentChar.getSkillList();
            skillLevel.setText(skills.get(0).getLvl()+"/99");
            skillXp.setText(skills.get(0).getXp()+" XP");
            currentTool.setText(skills.get(0).getCurrentTool().getName());
            int xpCap = skills.get(0).calculateXPCap(skills.get(0).getLvl());
        }
        return v;
    }

    public void updateUI() {
        if(currentChar.getSkillList().get(0).getXp()>=currentChar.getSkillList().get(0).calculateXPCap(currentChar.getSkillList().get(0).getLvl())&&currentChar.getSkillList().get(0).getLvl()!=100){
            currentChar.getSkillList().get(0).setLvl(currentChar.getSkillList().get(0).getLvl()+1);
            currentChar.getSkillList().get(0).setXp(0);
        }
        Skill treeCuttingSkill = currentChar.getSkillList().get(0);
        progressBar_level.setProgress((int) treeCuttingSkill.calculatePercentage(treeCuttingSkill.calculateXPCap(treeCuttingSkill.getLvl()),treeCuttingSkill.getXp()));
        skillXp.setText(treeCuttingSkill.getXp()+" XP");
        skillLevel.setText(treeCuttingSkill.getLvl()+"/99");
        currentChar.getSkillList().get(0).getMasteryPool().setMasteryMax(7000000);
        if(currentChar.getSkillList().get(0).getMasteryPool().getMasteryxp()!=currentChar.getSkillList().get(0).getMasteryPool().getMasteryMax()){
            currentChar.getSkillList().get(0).getMasteryPool().setMasteryxp(currentChar.getSkillList().get(0).getMasteryPool().getMasteryxp()+1);
            currentMasteryLevelNumber.setText(currentChar.getSkillList().get(0).getMasteryPool().getMasteryxp()+"/"+currentChar.getSkillList().get(0).getMasteryPool().getMasteryMax());
        }
        Skill treeSkill = currentChar.getSkillList().get(0);
        progressBar_level.setProgress((int) treeSkill.calculatePercentage(treeSkill.calculateXPCap(treeSkill.getLvl()),treeSkill.getXp()));
    }

    public TreeFragment() {
    }

    private void changeFragment(Fragment currentFragment) {
        getParentFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_container,currentFragment).commit();
    }
    private List<Tree> exampleTrees(){
        return new ArrayList<Tree>(){{
            add(new Tree("Arbre 1","100xp",1,500,1000,R.drawable.normal_tree));
            add(new Tree("Arbre 2","200xp",1,500,1000,R.drawable.normal_tree));
            add(new Tree("Arbre 3","300xp",1,500,1000,R.drawable.normal_tree));
            add(new Tree("Arbre 4","400xp",1,500,1000,R.drawable.normal_tree));
            add(new Tree("Arbre 5","500xp",1,500,1000,R.drawable.normal_tree));
            add(new Tree("Arbre 6","600xp",1,500,1000,R.drawable.normal_tree));
            add(new Tree("Arbre 7","700xp",1,500,1000,R.drawable.normal_tree));
        }};
    }
    
}