package cat.itb.melvoridle.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import cat.itb.melvoridle.Firebase.FirebaseHelper;
import cat.itb.melvoridle.Model.Character;
import cat.itb.melvoridle.R;

import static androidx.navigation.fragment.NavHostFragment.findNavController;


public class ChangeCharNameFragment extends Fragment {
TextInputLayout newNameLayout;
TextInputEditText newNameEdit;
Button confirmNewName;
public static Character currentChar;


    public ChangeCharNameFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_change_char_name, container, false);
        newNameLayout = v.findViewById(R.id.input_layout_new_name);
        newNameEdit = v.findViewById(R.id.input_text_new_name);
        confirmNewName = v.findViewById(R.id.changeNameButton);
        Bundle parametros = this.getArguments();
        if (parametros!=null){
            currentChar=parametros.getParcelable("character");
            newNameEdit.setText(currentChar.getName());
        }
        confirmNewName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newNameEdit.getText().toString().isEmpty()){
                    newNameLayout.setError("Campo vacio");
                } else {
                    newNameLayout.setError("Campo vacio");
                    String newName = newNameEdit.getText().toString();
                    currentChar.setName(newName);
                    FirebaseHelper firebaseHelper = new FirebaseHelper();
                    firebaseHelper.update(currentChar);
                    NavDirections reloadFragment = GameFragmentDirections.actionGameFragment2Self(currentChar);
                    Navigation.findNavController(v).navigate(reloadFragment);
                }
            }
        });
        return v;
    }
}