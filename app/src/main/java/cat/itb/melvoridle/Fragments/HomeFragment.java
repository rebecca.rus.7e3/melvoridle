package cat.itb.melvoridle.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import cat.itb.melvoridle.Firebase.FirebaseHelper;
import cat.itb.melvoridle.Model.Character;
import cat.itb.melvoridle.Model.Tool;
import cat.itb.melvoridle.R;
import cat.itb.melvoridle.RecyclerView.AdapterCharacter;


public class HomeFragment extends Fragment {
    Button confirmButton;
    LinearLayout termsAndConditions;
    LinearLayout character1;
    LinearLayout character2;
    LinearLayout character3;
    LinearLayout newChar;
    TextInputLayout characterNameLayout;
    TextInputEditText characterNameEditText;
    RadioGroup radioGroup;
    RadioButton normal;
    RadioButton hardcore;
    TextView gamemodeName;
    TextView gamemodeDescription;
    Button createCharacter;
    Button loginRegister;
    Button closeSession;
    boolean isLoginRegister;
    private RecyclerView recyclerView;
    private AdapterCharacter adapter;
    String idJugador;
    FirebaseHelper firebaseHelper;
    LinearLayout newCharacters;
    String gamemode;


    public HomeFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseHelper = new FirebaseHelper();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        termsAndConditions = v.findViewById(R.id.linear_layout_terms_conditions);
        character1 = v.findViewById(R.id.linear_layout_character1);
        character2 = v.findViewById(R.id.linear_layout_character2);
        character3 = v.findViewById(R.id.linear_layout_character3);
        newCharacters = v.findViewById(R.id.newCharactersLayouts);
        newChar = v.findViewById(R.id.newCharacter);
        confirmButton = v.findViewById(R.id.acceptButton);
        createCharacter = v.findViewById(R.id.createButton);
        closeSession = v.findViewById(R.id.closeSessionButton);
        closeSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                NavDirections homeToHome = HomeFragmentDirections.actionHomeFragmentSelf(false);
                Navigation.findNavController(v).navigate(homeToHome);
            }
        });
        characterNameLayout = v.findViewById(R.id.input_layout_name);
        characterNameEditText = v.findViewById(R.id.input_text_name);
        radioGroup = v.findViewById(R.id.radioGroup);
        normal = v.findViewById(R.id.radio_normal);
        hardcore = v.findViewById(R.id.radio_hardcore);
        gamemodeName = v.findViewById(R.id.gamemodeName);
        recyclerView = v.findViewById(R.id.recycler_view_character);
        gamemodeDescription = v.findViewById(R.id.gamemodeDescription);
        loginRegister= v.findViewById(R.id.loginRegisterButton);
        loginRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDirections homeToLogin = HomeFragmentDirections.homeToLogin();
                Navigation.findNavController(v).navigate(homeToLogin);
            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radio_normal:
                        gamemodeName.setVisibility(View.VISIBLE);
                        gamemodeDescription.setVisibility(View.VISIBLE);
                        gamemodeName.setText("Standard Mode");
                        gamemodeName.setTextColor(getResources().getColor(R.color.textColor));
                        gamemodeDescription.setText("Standard Combat modifiers. No bank space purchase limit. All skills unlocked.");
                        gamemodeDescription.setTextColor(getResources().getColor(R.color.textColor));
                        break;
                    case R.id.radio_hardcore:
                        gamemodeName.setVisibility(View.VISIBLE);
                        gamemodeDescription.setVisibility(View.VISIBLE);
                        gamemodeName.setText("Hardcore Mode");
                        gamemodeName.setTextColor(getResources().getColor(R.color.bonfire_inactive));
                        gamemodeDescription.setText("This is not a safe Gamemode. Character will be lost on death.");
                        gamemodeDescription.setTextColor(getResources().getColor(R.color.bonfire_inactive));
                        break;
                }
            }
        });
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                termsAndConditions.setVisibility(View.INVISIBLE);
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user!=null){
                    closeSession.setVisibility(View.VISIBLE);
                    Query query = firebaseHelper.getMyRef().orderByChild("idJugador").equalTo(user.getEmail());
                    FirebaseRecyclerOptions<Character> options = new FirebaseRecyclerOptions.Builder<Character>()
                            .setQuery(query, Character.class).build();
                    DatabaseReference reference = firebaseHelper.getMyRef();
                    reference.orderByChild("idJugador").equalTo(user.getEmail()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            if (snapshot.exists()){
                                Toast.makeText(getContext(),(int) snapshot.getChildrenCount()+"",Toast.LENGTH_SHORT).show();
                                int count = (int) snapshot.getChildrenCount();
                                switch (count){
                                    case 1:
                                        recyclerView.setVisibility(View.VISIBLE);
                                        newCharacters.setVisibility(View.VISIBLE);
                                        character1.setVisibility(View.INVISIBLE);
                                        character2.setVisibility(View.VISIBLE);
                                        character3.setVisibility(View.VISIBLE);
                                        break;
                                    case 2:
                                        recyclerView.setVisibility(View.VISIBLE);
                                        newCharacters.setVisibility(View.VISIBLE);
                                        character1.setVisibility(View.INVISIBLE);
                                        character2.setVisibility(View.INVISIBLE);
                                        character3.setVisibility(View.VISIBLE);
                                        break;
                                    case 3:
                                        recyclerView.setVisibility(View.VISIBLE);
                                        newCharacters.setVisibility(View.INVISIBLE);
                                        break;
                                }
                            } else {
                                recyclerView.setVisibility(View.INVISIBLE);
                                newCharacters.setVisibility(View.VISIBLE);
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                    adapter = new AdapterCharacter(options);
                    adapter.startListening();
                    adapter.setOnItemClickListener(new AdapterCharacter.OnItemClickListener() {
                        @Override
                        public void onItemClick(Character c, int position) {
                            NavDirections homeToTree = HomeFragmentDirections.homeToGame(c);
                            Navigation.findNavController(v).navigate(homeToTree);
                        }
                    });
                    recyclerView.setVisibility(View.VISIBLE);
                    isLoginRegister=true;
                    idJugador= user.getEmail();
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    loginRegister.setVisibility(View.INVISIBLE);
                } else {
                    isLoginRegister=false;
                    newCharacters.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.INVISIBLE);
                    loginRegister.setVisibility(View.VISIBLE);
                    closeSession.setVisibility(View.INVISIBLE);
                }
            }
        });
        character1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newCharacter();
            }
        });
        character2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newCharacter();
            }
        });
        character3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newCharacter();
            }
        });
        createCharacter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Character c;
                if (characterNameEditText.getText().toString().isEmpty()){
                    characterNameEditText.setError("Campo vacio!!!");
                } else {
                    String name = characterNameEditText.getText().toString();
                    if(radioGroup.getCheckedRadioButtonId()==-1)
                    {
                        Toast.makeText(getContext(), "Please select Gamemode", Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        // get selected radio button from radioGroup
                        int selectedId = radioGroup.getCheckedRadioButtonId();
                        switch (selectedId){
                            case R.id.radio_normal:
                                gamemode="Standard";
                                break;
                            case R.id.radio_hardcore:
                                gamemode="Hardcore";
                                break;
                        }
                        if (isLoginRegister){
                            c = new Character(name,idJugador, gamemode,"Cloud Save");
                            firebaseHelper.insert(c);
                        } else {
                            c = new Character(name,gamemode,"Local Save");
                            firebaseHelper.insert(c);
                        }
                        NavDirections homeToTree = HomeFragmentDirections.homeToGame(c);
                        Navigation.findNavController(v).navigate(homeToTree);
                    }
                }
            }
        });

        return v;
    }

    public void newCharacter(){
        newChar.setVisibility(View.VISIBLE);
        newCharacters.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
        loginRegister.setVisibility(View.INVISIBLE);
    }




}