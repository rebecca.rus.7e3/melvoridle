package cat.itb.melvoridle.Fragments;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import cat.itb.melvoridle.Model.Character;
import cat.itb.melvoridle.R;

import static androidx.navigation.fragment.NavHostFragment.findNavController;


public class GameFragment extends Fragment {

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ImageView topBar;
    ColorDrawable colorDrawable;
    Character currentChar;
    Menu optionsMenu;

    public GameFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_game, container, false);
        Toolbar myToolbar =
                v.findViewById(R.id.my_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(myToolbar);
        myToolbar.setNavigationIcon(R.drawable.ic_baseline_menu_24);
        drawerLayout = v.findViewById(R.id.drawer_layout);
        topBar = v.findViewById(R.id.imageViewTopApp);
        navigationView = v.findViewById(R.id.nvView);
        navigationView.setItemIconTintList(null);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawerLayout, myToolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setCheckedItem(R.id.item1);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectDrawerItem(item);
                return true;
            }
        });
        setHasOptionsMenu(true);
        if (getArguments()!=null) currentChar = getArguments().getParcelable("character");
        if (currentChar!=null){
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Wood Cutting");
            colorDrawable
                    = new ColorDrawable(Color.parseColor("#358F12"));
            ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
            Fragment fragment = null;
            Class fragmentClass;
            fragmentClass = TreeFragment.class;
            try {
                Bundle b = new Bundle();
                b.putParcelable("character",currentChar);
                fragment = (Fragment) fragmentClass.newInstance();
                fragment.setArguments(b);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e);
            }
            getFragmentManager().beginTransaction().replace(R.id.flContent, fragment).commit();
        }
        return v;
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the fragment to show based on nav item clicked
        Fragment fragment = null;
        Class fragmentClass;
        switch(menuItem.getItemId()) {
            case R.id.item1:
                fragmentClass = TreeFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Wood Cutting");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_woodcutting_header));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#358F12"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.item2:
                fragmentClass = FishFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Fishing");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_fishing_header));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#92D0F1"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.item3:
                fragmentClass = FireFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Firemaking");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_firemaking));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#B46624"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.item4:
                fragmentClass = CookingFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Cooking");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_cooking));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#C2B6B8"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.item5:
                fragmentClass = MiningFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Mining");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_mining));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#95857A"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.item6:
                fragmentClass = SmithingFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Smithing");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_smithing));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#95857A"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.item7:
                fragmentClass = EmptyFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Thieving");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_thieving));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#837F73"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.item8:
                fragmentClass = EmptyFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Farming");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_farming));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#AB921A"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.item9:
                fragmentClass = FletchingFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Fletching");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_fletching));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#E87575"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.item10:
                fragmentClass = CraftingFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Crafting");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_crafting));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#947650"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.item11:
                fragmentClass = RuneCraftingFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("RuneCrafting");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_runecrafting));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#D2B2A6"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.item12:
                fragmentClass = EmptyFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Herblore");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_herblore));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#45C094"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.item13:
                fragmentClass = EmptyFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Agility");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_agility));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#2C87FA"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.item14:
                fragmentClass = AltMagicFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Alt Magic");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_magic));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#8D7BCA"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.bank:
                fragmentClass = InventoryFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Bank");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_bank));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#B57E3B"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            case R.id.shop:
                fragmentClass = ShopFragment.class;
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Shop");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_gp));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#FEBB33"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                break;
            default:
                fragmentClass = EmptyFragment.class;
        }
        try {
            Bundle b = new Bundle();
            b.putParcelable("character",currentChar);
            fragment = (Fragment) fragmentClass.newInstance();
            fragment.setArguments(b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        getFragmentManager().beginTransaction().replace(R.id.flContent, fragment).commit();
        menuItem.setChecked(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user!=null){
            inflater.inflate(R.menu.top_app_bar_menu_online, menu);
            MenuItem item = menu.findItem(R.id.loginOnline);
            item.setTitle("Logged in as: "+user.getEmail());
        } else {
            inflater.inflate(R.menu.top_app_bar_menu_offline, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
        optionsMenu=menu;
        MenuItem item = menu.findItem(R.id.menu_search_option);
        item.setTitle(currentChar.getName()+" v");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.signOUT:
                FirebaseAuth.getInstance().signOut();
                NavDirections gameToHomw = GameFragmentDirections.gameToHome(false);
                findNavController(this).navigate(gameToHomw);
                return true;
            case R.id.actionsChangeChar:
                NavDirections gameToHome = GameFragmentDirections.gameToHome(true);
                findNavController(this).navigate(gameToHome);
                return true;
            case R.id.login:
                NavDirections gameToLogin = GameFragmentDirections.gameToLogin();
                findNavController(this).navigate(gameToLogin);
                return true;
            case R.id.actionsChangeName:
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Settings");
                topBar.setImageDrawable(getResources().getDrawable(R.drawable.ic_settings_header));
                colorDrawable
                        = new ColorDrawable(Color.parseColor("#BCC0C0"));
                ((AppCompatActivity) getActivity()).getSupportActionBar().setBackgroundDrawable(colorDrawable);
                Fragment fragment = new ChangeCharNameFragment();
                try {
                    Bundle b = new Bundle();
                    b.putParcelable("character",currentChar);
                    fragment.setArguments(b);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getFragmentManager().beginTransaction().replace(R.id.flContent, fragment).commit();
            default:
                return super.onContextItemSelected(item);
        }
    }
}