package cat.itb.melvoridle.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cat.itb.melvoridle.Model.Character;
import cat.itb.melvoridle.Model.Item;
import cat.itb.melvoridle.Model.Mineral;
import cat.itb.melvoridle.Model.Skill;
import cat.itb.melvoridle.Model.Tree;
import cat.itb.melvoridle.R;
import cat.itb.melvoridle.RecyclerView.AdapterMining;
import cat.itb.melvoridle.RecyclerView.AdapterTreeCutting;

public class MiningFragment extends Fragment {

    private List<Mineral> minerals;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    DrawerLayout drawerLayout;
    public static Character currentChar;

    TextView skillLevel;
    TextView skillXp;
    TextView currentTool;
    ProgressBar progressBar_level;
    ProgressBar currentMasteryLevelProgress;
    TextView currentMasteryLevelNumber;

    List<Item> inventory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_mining,container,false);

        minerals = exampleTrees();

        recyclerView = v.findViewById(R.id.recycler_view_mining);
        layoutManager = new LinearLayoutManager(getContext());
        adapter = new AdapterMining(getContext(),minerals,R.layout.list_item_mining,new AdapterMining.OnItemClickListener(){


            @Override
            public void onItemClick(Tree tree, int position) {

            }
        },this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        Bundle parametros = this.getArguments();

        skillLevel = v.findViewById(R.id.skill_level_level_mining);
        skillXp = v.findViewById(R.id.skill_xp_xp_mining);
        currentTool = v.findViewById(R.id.current_tool_name_mining);
        progressBar_level = v.findViewById(R.id.current_level_progress_mining_fragment);
        currentMasteryLevelProgress = v.findViewById(R.id.current_mastery_level_progress_mining_fragment);
        currentMasteryLevelNumber = v.findViewById(R.id.current_mastery_level_number_mining);


        if (parametros!=null){
            currentChar = parametros.getParcelable("character");
            inventory = currentChar.getInventory();
            List<Skill> skills = currentChar.getSkillList();
            skillLevel.setText(skills.get(4).getLvl()+"/99");
            skillXp.setText(skills.get(4).getXp()+" XP");
            currentTool.setText(skills.get(4).getCurrentTool().getName());
            int xpCap = skills.get(4).calculateXPCap(skills.get(4).getLvl());
            progressBar_level.setProgress((int) skills.get(4).calculatePercentage(xpCap,skills.get(4).getXp()));
        }

        return v;
    }

    public void updateUI() {
        if(currentChar.getSkillList().get(4).getXp()>=currentChar.getSkillList().get(4).calculateXPCap(currentChar.getSkillList().get(4).getLvl())&&currentChar.getSkillList().get(4).getLvl()!=100){
            currentChar.getSkillList().get(4).setLvl(currentChar.getSkillList().get(4).getLvl()+1);
            currentChar.getSkillList().get(4).setXp(0);
        }
        Skill miningSkill = currentChar.getSkillList().get(4);
        progressBar_level.setProgress((int) miningSkill.calculatePercentage(miningSkill.calculateXPCap(miningSkill.getLvl()),miningSkill.getXp()));
        skillXp.setText(miningSkill.getXp()+" XP");
        skillLevel.setText(+miningSkill.getLvl()+"/99");
    }

    public MiningFragment(){}

    private void changeFragment(Fragment currentFragment) {
        getParentFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_container,currentFragment).commit();
    }
    private List<Mineral> exampleTrees(){
        return new ArrayList<Mineral>(){{
            add(new Mineral("Mineral 1","1xp/10s",1,500,1000,R.drawable.rock_copper));
            add(new Mineral("Mineral 2","1xp/10s",1,500,1000,R.drawable.rock_copper));
            add(new Mineral("Mineral 3","1xp/10s",1,500,1000,R.drawable.rock_copper));
            add(new Mineral("Mineral 4","1xp/10s",1,500,1000,R.drawable.rock_copper));
            add(new Mineral("Mineral 5","1xp/10s",1,500,1000,R.drawable.rock_copper));
            add(new Mineral("Mineral 6","1xp/10s",1,500,1000,R.drawable.rock_copper));
            add(new Mineral("Mineral 7","1xp/10s",1,500,1000,R.drawable.rock_copper));
            add(new Mineral("Mineral 8","1xp/10s",1,500,1000,R.drawable.rock_copper));
            add(new Mineral("Mineral 9","1xp/10s",1,500,1000,R.drawable.rock_copper));
            add(new Mineral("Mineral 10","1xp/10s",1,500,1000,R.drawable.rock_copper));
        }};
    }
}
