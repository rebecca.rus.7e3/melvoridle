package cat.itb.melvoridle.Fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import cat.itb.melvoridle.R;

import static android.content.ContentValues.TAG;


public class RegisterFragment extends Fragment {
    Button registerButton;
    ImageButton backtoLogin;
    private FirebaseAuth mAuth;
    TextInputEditText usernameEditText;
    TextInputEditText passwordEditText;
    TextInputEditText repeatPasswordEditText;
    TextInputEditText emailEditText;
    TextInputLayout passwordLayout;
    TextInputLayout repeatPasswordLayout;


    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_register, container, false);
        registerButton = v.findViewById(R.id.regButton);
        usernameEditText = v.findViewById(R.id.editTextUserName);
        passwordEditText = v.findViewById(R.id.editTextPassword);
        repeatPasswordEditText = v.findViewById(R.id.editTextRepPassword);
        emailEditText = v.findViewById(R.id.editTextEmail);
        passwordLayout = v.findViewById(R.id.input_layout_password);
        repeatPasswordLayout = v.findViewById(R.id.input_layout_repPassword);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (checkRegister()) {
                    String email = emailEditText.getText().toString();
                    String password = passwordEditText.getText().toString();
                    mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getContext(), "Registro completado", Toast.LENGTH_SHORT).show();
                                NavDirections registerToLogin = RegisterFragmentDirections.registerToLogin();
                                Navigation.findNavController(v).navigate(registerToLogin);
                            } else {
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                Toast.makeText(getContext(), "Registro NO completado", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                }

            }
        });
        backtoLogin = v.findViewById(R.id.backToLogin);
        backtoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDirections registerToLogin = RegisterFragmentDirections.registerToLogin();
                Navigation.findNavController(v).navigate(registerToLogin);
            }
        });

        return v;
    }

    public boolean checkRegister() {
        if (usernameEditText.getText().toString().isEmpty()) {
            usernameEditText.setError("Campo vacio!!!");
            return false;
        } else {
            if (passwordEditText.getText().toString().isEmpty()) {
                passwordLayout.setError("Campo vacio!!!");
                return false;
            } else {
                passwordLayout.setError("");
                if (repeatPasswordEditText.getText().toString().isEmpty()) {
                    repeatPasswordLayout.setError("Campo vacio!!!");
                    return false;
                } else {
                    repeatPasswordLayout.setError("");
                    if (passwordEditText.getText().toString().length() < 6) {
                        passwordLayout.setError("Minimo 6 caracteres");
                        return false;
                    } else {
                        if (emailEditText.getText().toString().isEmpty()) {
                            emailEditText.setError("Campo vacio!!!");
                            return false;
                        } else {
                            if (!repeatPasswordEditText.getText().toString().equals(passwordEditText.getText().toString())) {
                                repeatPasswordLayout.setError("No son iguales las contraseñas");
                                return false;
                            }

                        }
                        return true;
                    }
                }
            }
        }


    }
}