package cat.itb.melvoridle.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import cat.itb.melvoridle.Firebase.FirebaseHelper;
import cat.itb.melvoridle.Model.Character;
import cat.itb.melvoridle.Model.Item;
import cat.itb.melvoridle.R;
import cat.itb.melvoridle.RecyclerView.AdapterInventory;


public class InventoryFragment extends Fragment {
List<Item> itemList;
private RecyclerView recyclerView;
private RecyclerView.Adapter adapter;
private RecyclerView.LayoutManager layoutManager;
public Character currentCharacter;
public LinearLayout infoLayout;
TextView itemSpace, itemName, itemDesc, itemPrice, itemMaxQuantity, quantityToSell, GpPlayer;
Item currentItem;
Button sellButton, allButton, allButOneButton;
SeekBar sellBar;
public int quantity;
private FirebaseHelper firebaseHelper;



    public InventoryFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_inventory, container, false);
        firebaseHelper = new FirebaseHelper();
        Bundle parametros = this.getArguments();
        if (parametros != null) {
            currentCharacter= parametros.getParcelable("character");
            itemList = currentCharacter.getInventory();
            removePlaceHolder();
            currentCharacter.setInventory(itemList);
            firebaseHelper.update(currentCharacter);
        }

        recyclerView = v.findViewById(R.id.recycler_view_inventory);
        itemSpace = v.findViewById(R.id.bank_space_used);
        GpPlayer = v.findViewById(R.id.gp_value);
        GpPlayer.setText(currentCharacter.getGp()+"");
        infoLayout = v.findViewById(R.id.linear_layout_item_info);
        itemName = v.findViewById(R.id.itemNameInfo);
        itemDesc = v.findViewById(R.id.itemDescInfo);
        itemPrice = v.findViewById(R.id.itemPriceInfo);
        quantityToSell = v.findViewById(R.id.quantitySell);
        sellBar = v.findViewById(R.id.sellSeekBar);
        sellBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                quantityToSell.setText(String.valueOf(progress));
                quantity=progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                quantityToSell.setText("1");
                quantity=1;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        itemMaxQuantity = v.findViewById(R.id.itemMaxQuantity);
        itemSpace.setText(itemList.size()+"/12");
        layoutManager = new GridLayoutManager(getContext(),5);
        adapter = new AdapterInventory(new AdapterInventory.OnItemClickListener() {
            @Override
            public void onItemClick(Item i, int position) {
                infoLayout.setVisibility(View.VISIBLE);
                itemName.setText(i.getName());
                itemDesc.setText(i.getDescription());
                itemPrice.setText(i.getPrice()+"");
                itemMaxQuantity.setText(i.getQuantity()+"");
                currentItem=i;
                sellBar.setMax(i.getQuantity());
            }
        }, itemList,R.layout.item_view_inventory);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        sellButton = v.findViewById(R.id.sellButton);
        sellButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentItem.setQuantity(currentItem.getQuantity()-quantity);
                int moneyGain = quantity*currentItem.getPrice();
                currentCharacter.setGp(currentCharacter.getGp()+moneyGain);
                if (currentItem.getQuantity()==0){
                    infoLayout.setVisibility(View.INVISIBLE);
                    itemList.remove(currentItem);
                    currentCharacter.setInventory(itemList);
                } else {
                    UpdateUI();
                }
                GpPlayer.setText(currentCharacter.getGp()+"");
                adapter.notifyDataSetChanged();
                firebaseHelper.update(currentCharacter);
            }
        });
        allButton = v.findViewById(R.id.buttonAll);
        allButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sellBar.setProgress(currentItem.getQuantity());
            }
        });
        allButOneButton = v.findViewById(R.id.buttonAllBut1);
        allButOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentItem.getQuantity()>1){
                    sellBar.setProgress(currentItem.getQuantity()-1);
                } else {
                    sellBar.setProgress(currentItem.getQuantity());
                }
            }
        });
        return v;
    }

    public void UpdateUI(){
        sellBar.setMax(currentItem.getQuantity());
        sellBar.setProgress(1);
        itemMaxQuantity.setText(currentItem.getQuantity()+"");

    }

    public void removePlaceHolder(){
        Item itemToDelete = null;
        for (int i = 0;i<itemList.size();i++){
            if (itemList.get(i).getName().equalsIgnoreCase("placeholder")){
                itemToDelete=itemList.get(i);
            }
        }
        if (itemToDelete!=null){
            itemList.remove(itemToDelete);
        }

    }
}