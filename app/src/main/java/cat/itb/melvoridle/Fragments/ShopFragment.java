package cat.itb.melvoridle.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cat.itb.melvoridle.Firebase.FirebaseHelper;
import cat.itb.melvoridle.Model.Character;
import cat.itb.melvoridle.Model.Item;
import cat.itb.melvoridle.Model.Tool;
import cat.itb.melvoridle.R;
import cat.itb.melvoridle.RecyclerView.AdapterShop;


public class ShopFragment extends Fragment {
    List<Item> itemList, inventory;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    public Character currentCharacter;
    private FirebaseHelper firebaseHelper;
    TextView playerGP;
    public int capacity=12;




    public ShopFragment() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.fragment_shop, container, false);
        firebaseHelper = new FirebaseHelper();
        Bundle parametros = this.getArguments();
        playerGP = v.findViewById(R.id.gp_player);
        if (parametros != null) {
            currentCharacter= parametros.getParcelable("character");
            playerGP.setText(currentCharacter.getGp()+" GP");
        }
        itemList=addShopItems();
        recyclerView= v.findViewById(R.id.shopRecycler);
        layoutManager = new LinearLayoutManager(getContext());
        adapter = new AdapterShop(new AdapterShop.OnItemClickListener() {
            @Override
            public void onItemClick(Item i, int position) {
                switch (i.getName()){
                    case "Extra Bank Slot":
                        if (currentCharacter.getGp()>i.getPrice()){
                            currentCharacter.setGp(currentCharacter.getGp()-i.getPrice());
                            playerGP.setText(currentCharacter.getGp()+" GP");
                            inventory=currentCharacter.getInventory();
                            capacity++;
                            i.setPrice(i.getPrice()+10);
                            List<Item> newCapacity = new ArrayList<>(capacity);
                            for (int in=0;in<inventory.size();in++){
                                newCapacity.add(inventory.get(in));
                            }
                            currentCharacter.setInventory(newCapacity);
                            firebaseHelper.update(currentCharacter);
                            adapter.notifyDataSetChanged();
                        }
                        break;
                    case "Iron Axe":
                        if (currentCharacter.getGp()>i.getPrice()){
                            currentCharacter.setGp(currentCharacter.getGp()-i.getPrice());
                            playerGP.setText(currentCharacter.getGp()+" GP");
                            currentCharacter.getSkillList().get(0).setCurrentTool(new Tool("Iron Axe","No bonuses",0,1,0,"Woodcutting",10));
                            firebaseHelper.update(currentCharacter);
                            adapter.notifyDataSetChanged();
                        }
                        break;
                }
            }
        },itemList,R.layout.shop_item);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        return v;
    }

    public List<Item> addShopItems(){
        return new ArrayList<Item>(){{
            add(new Item("Extra Bank Slot","+1 Maximum Bank Space   ",R.drawable.ic_bank,34,1));
            add(new Item("Iron Axe","Get iron axe for woodcutting",R.drawable.ic_bank,100,1));
        }};
    }
}